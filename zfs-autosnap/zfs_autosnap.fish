complete -c zfs_autosnap
complete -c zfs_autosnap -f
complete -c zfs_autosnap -s f -l frequency -d "Snap frequency label" -xa "hourly daily weekly monthly yearly"
complete -c zfs_autosnap -a '( zfs list -t filesystem -o name -H)' -d "ZFS filesystem"
complete -c zfs_autosnap -s r -l recursive -d "Create a snapshot for all children"

