# Clone this repository

# Build dependencies
apk add alpine-sdk

# Create apk for a package
cd `pkgdir`
abuild checksum
abuild -r

# create update/create an index of packges in a repository
apk index -vU -o APKINDEX.tar.gz --rewrite-arch x86_64 *.apk
abuild-sign APKINDEX.tar.gz

cd ~/packages/alpine/x86_64/; and apk index --rewrite-arch x86_64  -vU -o APKINDEX.tar.gz *.apk; and abuild-sign APKINDEX.tar.gz

# install the package
apk add --repository /home/shervin/packages/alpine/  $pkgname

# copy local repository to freebox
rclone copy ~/packages/alpine/x86_64/ mafreebox:/Disque\ dur/apk_repo/x86_64
