#!/bin/sh
# vim: set ts=4 sw=4:
#
# This is the default ACPI handler script that is configured in
# /etc/acpi/events/anything to be called for every ACPI event.
# You can edit it and add your own actions; treat it as a configuration file.
#
# lid-closed, power-supply-ac and suspend are scripts located in
# /usr/share/acpid/.
#
PATH="/usr/share/acpid:$PATH:/etc/acpi/:/etc/acpi/scripts"
alias log='logger -t acpid'

# <dev-class>:<dev-name>:<notif-value>:<sup-value>
case "$1:$2:$3:$4" in

button/power:PWRF:* | button/power:PBTN:*)
	log 'Power button pressed'
	# If we have a lid (notebook), suspend to RAM, otherwise power off.
	if [ -e /proc/acpi/button/lid/LID ]; then
		runuser -u shervin i3lock && suspend
	else
		poweroff
	fi
	;;
ibm/hotkey:LEN*:00004011)
    log 'Dock disconnected'
    external_monitor
    wifi_toggle
    ;;
ibm/hotkey:LEN0068*:00004010)
    log 'Dock connected'
    sleep 2s
    wifi_toggle
    external_monitor
    ;;
#
button/sleep:SLPB:*)
	log 'Sleep button pressed'
	runuser -u shervin i3lock &&  suspend
;;
button/lid:*:close:*)
	log 'Lid closed'

	# Suspend to RAM if AC adapter is not connected.
	#power-supply-ac || suspend

	# always suspend
	suspend 
	;;
button/lid:*:open:*)
    log 'Lid opened'
    external_monitor
    wifi_toggle
    ;;
ac_adapter:*:*:*0)
	log 'AC adapter unplugged'
	# Suspend to RAM if notebook's lid is closed.
	lid-closed && runuser -u shervin i3lock &&  suspend
	;;
video/brightnessup*)
    log "Brightness up"
    brightness up
    ;;
video/brightnessdown*)
    log "Brightness down"
    brightness down
    ;;
button/wlan*)
    log "WLAN switch"
    if command nmcli >/dev/null; then
	wifi_status=`nmcli radio wifi`
	if test $wifi_status == "enabled"; then
	    nmcli radio wifi off
	else
	    nmcli radio wifi on
	fi
    else
	wifi_toggle
    fi
    ;;
video/switchmode:VMOD*)
    log 'Monitor switch'
    external_monitor
    ;;
esac

exit 0
# battery PNP0C0A:01 00000080 00000001
# ibm/hotkey LEN0068:00 00000080 00004011
# ac_adapter ACPI0003:00 00000080 00000000
# ibm/hotkey LEN0068:00 00000080 00006030
# thermal_zone LNXTHERM:00 00000081 00000000



# ibm/hotkey LEN0068:00 00000080 00004010
# battery PNP0C0A:01 00000080 00000001
# ac_adapter ACPI0003:00 00000080 00000001
# ibm/hotkey LEN0068:00 00000080 00006030
# thermal_zone LNXTHERM:00 00000081 00000000


#ibm/hotkey LEN0068:00 00000080 00006030
#button/lid LID open
#thermal_zone LNXTHERM:00 00000081 00000000
#battery PNP0C0A:01 00000080 00000001
# shervin
